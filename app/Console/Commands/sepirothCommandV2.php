<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;
class sepirothCommandV2 extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sepiroth:build2 {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This build a scaffolding for grid,form,and controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        /* creating the model */
        $name = $this->parseName($this->getNameInput());

        $path = $this->getPath($name);
        $model_exist = false;
        $model_form_exist = false;
        $model_grid_exist = false;
        //$this->info($this->getDefaultNamespace($this->laravel->getNamespace()));
        
            $name = $this->parseName($this->getNameInput().'/'.$this->getNameInput());
            $namespace =  $this->parseName($this->getNameInput());
            $base_model_path = $this->getModelBasePath();
            $model_path = $this->getModelPath($name);

            $this->makeDirectory($model_path);
            
            /* Creating Model */
            $this->info("Creating model....");
            $this->info($name);
            $this->info("Model base path...");
            $this->info($base_model_path);
            $this->info("Model path...");
            $this->info($model_path);
            $this->info("==================================");
            
            if (!$this->files->exists($model_path)) {

                $this->info('creating ' . $namespace . ' on path : ' . $base_model_path . '/' . $name);
                $this->files->put($model_path, $this->doBuildClass($namespace,''));
            }else{
                $this->error($this->getPath($name).' already exists!');
            }
            $this->info("==================================");
            $this->info("Creating grid model....");
            $this->info($name.'Grid');

            $model_path = $this->getModelPath($name.'Grid');

            if (!$this->files->exists($this->getPath($name.'Grid'))) {  
                $this->info('creating ' . $namespace . 'Grid on path : ' . $base_model_path . '/' . $name);
                $this->files->put($model_path, $this->doBuildClass($namespace,'grid'));
                $this->info($model_path);
            }else{
                $this->error($this->getPath($name.'Grid').' already exists!');
            }
            
            $this->info("==================================");
            $this->info("Creating form model....");
            $this->info($name.'Form');

            $model_path = $this->getModelPath($name.'Form');

            if (!$this->files->exists($this->getPath($name.'Form'))) {  
                $this->info('creating ' . $namespace . 'Form on path : ' . $base_model_path . '/' . $name);
                $this->files->put($model_path, $this->doBuildClass($namespace,'form'));
                $this->info($model_path);
            }else{
                $this->error($this->getPath($name.'Form').' already exists!');
            }

            $controllernamespace = "App\Http\Controllers";
            $controllername = $this->getNameInput();
            $controllerpath = $this->laravel['path'] . "/Http/Controllers/" . $controllername;
            
            $this->info($controllernamespace);
            $this->info($controllername);
            $this->info($controllerpath);
            if (!$this->files->exists($controllerpath)) { 
                $this->info('creating ' . $controllername . ' on path : ' . $controllerpath);

                $model = $this->getNameInput();
                $gridname = $this->getNameInput().'Grid';
                $formname = $this->getNameInput().'Form';

                $stub = $this->files->get($this->getControllerStub());
                $stub = $this->replaceDummyModel($stub, $model);
                $stub = $this->replaceDummyGrid($stub,$gridname);
                $stub = $this->replaceDummyForm($stub,$formname);
                $stub = $this->replaceDummyRoute($stub, strtolower($model));

                $this->files->put($controllerpath.'Controller.php', $stub);
            }else{
                $this->error($controllerpath.' already exists!');
            }


            $this->info($name.' created successfully.');
            $this->info("==================================");

            $this->info("Paste this following routes: routes/web.php");
            $this->info("Route::get('/admin/".strtolower($this->getNameInput())."', '".$this->getNameInput()."Controller@index')->name('admin.".strtolower($this->getNameInput())."');");
            $this->info("Route::get('/admin/".strtolower($this->getNameInput())."/add', '".$this->getNameInput()."Controller@add')->name('admin.".strtolower($this->getNameInput()).".add');");
            $this->info("Route::get('/admin/".strtolower($this->getNameInput())."/edit', '".$this->getNameInput()."Controller@edit')->name('admin.".strtolower($this->getNameInput()).".edit');");
            $this->info("Route::post('/admin/".strtolower($this->getNameInput())."/save', '".$this->getNameInput()."Controller@save')->name('admin.".strtolower($this->getNameInput()).".save');");
            
            $this->call('make:migration', ['name' => " create_".strtolower($this->getNameInput())."_table",'--create' => strtolower($this->getNameInput())
            ]);
            
    }
    protected function getStub()
    {
        return __DIR__.'/Sepiroth/model.stub';
    }
    
    protected function getGridStub()
    {
        return __DIR__.'/Sepiroth/modelgrid.stub';
    }
    
    protected function getFormStub()
    {
        return __DIR__.'/Sepiroth/modelform.stub';
    }
    protected function getControllerStub()
    {
        return __DIR__.'/Sepiroth/controller.stub';
    }
    protected function alreadyExists($rawName)
    {
        $name = $this->parseName($rawName);
        $this->info('checking if exists');
        $this->info($name);
        return $this->files->exists($this->getPath($name));
    }
    protected function makeDirectory($path)
    {
        
        if (! $this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);   
        }
    }
    
    protected function getDefaultNamespace($rootNamespace)
    {
        $model = $this->argument('name');
        return $rootNamespace . '\Model';
    }

    protected function doBuildClass($name,$type)
    {
        $stub = "";
        if($type==''){
            $stub = $this->files->get($this->getStub());
        }
        if($type=='grid'){
            $stub = $this->files->get($this->getGridStub());
        }
        if($type=='form'){
            $stub = $this->files->get($this->getFormStub());
        }
        

        $stub = $this->replaceDummyTable($stub, $name);
        $stub = $this->replaceDummyRoute($stub, $name);

        return $this->replaceNamespace($stub, $name)
                    ->replaceClass($stub, $name);
                    
    }

    protected function replaceDummyTable($stub, $name)
    {
        $table = strtolower($this->getNameInput());

        return str_replace('DummyTableName', $table, $stub);
    }
    protected function replaceDummyRoute($stub, $name)
    {
        $route = strtolower($this->getNameInput());

        return str_replace('DummyRoute', $route, $stub);
    }
    protected function replaceDummyModel($stub, $model)
    {
        return str_replace('DummyModel', $model, $stub);
    }
    protected function replaceDummyGrid($stub, $grid)
    {
        return str_replace('DummyGrid', $grid, $stub);
    }
    protected function replaceDummyForm($stub, $form)
    {
        return str_replace('DummyForm', $form, $stub);
    }
    
    public function parseName($name)
    {
        return trim(str_replace(array('export ', '\'', '"'), '', $name));
    }
    public function getModelPath($name=null){
        $name = $this->parseName($name);
        return $this->getPath('Model/'.$name);
    }
    public function getModelBasePath(){
        return app_path() . '/Model';
    }
}
