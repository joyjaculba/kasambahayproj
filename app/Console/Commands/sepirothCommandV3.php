<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;
class sepirothCommandV3 extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sepiroth:build3 {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is version 3 of building a scaffolding for grid,form,and controller. -Joy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        // $jsonString = Storage::disk('root')->get($this->getNameInput());
        $jsonString = $this->files->get(base_path()."/".$this->getNameInput());
        $data = json_decode($jsonString);
        foreach($data->tables as $table){
            

            $model_class = $table->model_class;

            $table_name = $table->table_name;

            $path = $this->getPath($model_class);
            $model_exist = false;
            $model_form_exist = false;
            $model_grid_exist = false;
        //$this->info($this->getDefaultNamespace($this->laravel->getNamespace()));
        
            // $name = $this->parseName($this->getNameInput().'/'.$this->getNameInput());
            $name = $this->parseName($table->model_class.'/'.$table->model_class);
            // $namespace =  $this->parseName($this->getNameInput());
            $namespace =  $this->parseName($table->model_class);
            $base_model_path = $this->getModelBasePath();
            $model_path = $this->getModelPath($name);

            $this->makeDirectory($model_path);
            
            /* Creating Model */
            $this->info("Creating model....");
            $this->info($name);
            $this->info("Model base path...");
            $this->info($base_model_path);
            $this->info("Model path...");
            $this->info($model_path);
            $this->info("==================================");


            if (!$this->files->exists($model_path)) {

                $this->info('creating ' . $namespace . ' on path : ' . $base_model_path . '/' . $name);
                $this->files->put($model_path, $this->doBuildClass($namespace,''));
            }else{
                $this->error($this->getPath($name).' already exists!');
            }
            $this->info("==================================");
            $this->info("Creating grid model....");
            $this->info($name.'Grid');

            $model_path = $this->getModelPath($name.'Grid');

            if (!$this->files->exists($this->getModelPath($name.'Grid'))) {  
                $this->info('creating ' . $namespace . 'Grid on path : ' . $base_model_path . '/' . $name);
                $this->files->put($model_path, $this->doBuildClass($namespace,'grid'));
                $this->info($model_path);
            }else{
                $this->error($this->getModelPath($name.'Grid').' already exists!');
            }
            
            $this->info("==================================");
            $this->info("Creating form model....");
            $this->info($name.'Form');

            $model_path = $this->getModelPath($name.'Form');

            if (!$this->files->exists($this->getModelPath($name.'Form'))) {  
                $this->info('creating ' . $namespace . 'Form on path : ' . $base_model_path . '/' . $name);
                $this->files->put($model_path, $this->doBuildClass($namespace,'form'));
                $this->info($model_path);
            }else{
                $this->error($this->getModelPath($name.'Form').' already exists!');
            }

            $controllernamespace = "App\Http\Controllers";
            // $controllername = $this->getNameInput();
            $controllername = $model_class;
            $controllerpath = $this->laravel['path'] . "/Http/Controllers/" . $controllername;
            
            $this->info($controllernamespace);
            $this->info($controllername);
            $this->info($controllerpath);

            if (!$this->files->exists($controllerpath."Controller.php")) { 
                $this->info('creating ' . $controllername . ' on path : ' . $controllerpath);

                // $model = $this->getNameInput();
                // $gridname = $this->getNameInput().'Grid';
                // $formname = $this->getNameInput().'Form';
                $model = $model_class;
                $gridname = $model_class.'Grid';
                $formname = $model_class.'Form';

                $stub = $this->files->get($this->getControllerStub());
                $stub = $this->replaceDummyModel($stub, $model);
                $stub = $this->replaceDummyGrid($stub,$gridname);
                $stub = $this->replaceDummyForm($stub,$formname);
                $stub = $this->replaceDummyRoute($stub, strtolower($model));

                $this->files->put($controllerpath.'Controller.php', $stub);
            }else{
                $this->error($controllerpath.' already exists!');
            }

            $this->info($model_class.' created successfully.');
            $this->info("==================================");

            $this->warn("For ".$model_class." - paste this following routes: routes/web.php");
            $this->line("Route::get('/admin/".strtolower($table_name)."', '".$model_class."Controller@index')->name('admin.".strtolower($table_name)."');");
            $this->line("Route::get('/admin/".strtolower($table_name)."/add', '".$model_class."Controller@add')->name('admin.".strtolower($table_name).".add');");
            $this->line("Route::get('/admin/".strtolower($table_name)."/edit/{id}', '".$model_class."Controller@edit')->name('admin.".strtolower($table_name).".edit');");
            $this->line("Route::post('/admin/".strtolower($table_name)."/save', '".$model_class."Controller@save')->name('admin.".strtolower($table_name).".save');");
            $this->line("Route::post('/admin/".strtolower($table_name)."/update/{id}', '".$model_class."Controller@update')->name('admin.".strtolower($table_name).".update');");

            // $fullpath = $this->call('sepiroth_make:migration', ['name' => " create_".strtolower($table_name)."_table",'--fullpath' => strtolower($table_name)
            // ]);
            Artisan::call('sepiroth_make:migration', ['name' => " create_".strtolower($table_name)."_table",'--fullpath' => strtolower($table_name)
            ]);
            $path_migration = str_replace("\n","",Artisan::output());

            // $path_migration = base_path()."/database/migrations/".$file_migration;
            $new = "\n";
            foreach($table->column_field as $columns){
                $new = $new ."\t \t \t"."$"."table->".$columns->type."('".$columns->value."');\n";
            }
            if($this->files->exists($path_migration)){
                // $this->line("YES");

                $stub = $this->files->get($this->getMigrationStub());
                // $this->files->chmod($path_migration,0777);
                $stub = $this->replaceDummyTable($stub, $table_name);
                $stub = str_replace('DummyMigration','Create'.ucfirst($table_name).'Table', $stub);
                $stub = $this->insertColumnFields($stub, $new);
                $this->files->replace($path_migration, $stub);

            }
            $this->info("==================================");
            $this->line("Created a migration done: ".$path_migration);
            $this->info("==================================");

            
           
        }     

         
    }
    protected function getStub()
    {
        return __DIR__.'/Sepiroth/model.stub';
    }
    
    protected function getGridStub()
    {
        return __DIR__.'/Sepiroth/modelgrid.stub';
    }
    
    protected function getFormStub()
    {
        return __DIR__.'/Sepiroth/modelform.stub';
    }
    protected function getControllerStub()
    {
        return __DIR__.'/Sepiroth/controller.stub';
    }
    protected function getMigrationStub()
    {
        return __DIR__.'/Sepiroth/migration.stub';
    }
    protected function alreadyExists($rawName)
    {
        $name = $this->parseName($rawName);
        $this->info('checking if exists');
        $this->info($name);
        return $this->files->exists($this->getPath($name));
    }
    protected function makeDirectory($path)
    {
        
        if (! $this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);   
        }
    }
    
    protected function getDefaultNamespace($rootNamespace)
    {
        $model = $this->argument('name');
        return $rootNamespace . '\Model';
    }

    protected function doBuildClass($name,$type)
    {
        $stub = "";
        if($type==''){
            $stub = $this->files->get($this->getStub());
        }
        if($type=='grid'){
            $stub = $this->files->get($this->getGridStub());
        }
        if($type=='form'){
            $stub = $this->files->get($this->getFormStub());
        }
        

        $stub = $this->replaceDummyTable($stub, $name);
        $stub = $this->replaceDummyRoute($stub, $name);

        return $this->replaceNamespace($stub, $name)
                    ->replaceClass($stub, $name);
                    
    }

    protected function replaceDummyTable($stub, $name)
    {
        // $table = strtolower($this->getNameInput());
        // $jsonString = $this->files->get(base_path()."/".$this->getNameInput());
        // $data = json_decode($jsonString, true);

        // $table_name = $data['table']['model_class'];

        $table = strtolower($name);
        

        return str_replace('DummyTableName', $table, $stub);
    }
    protected function replaceDummyRoute($stub, $name)
    {
        // $jsonString = $this->files->get(base_path()."/".$this->getNameInput());
        // $data = json_decode($jsonString, true);

        // $table_name = $data['table']['table_name'];
        $route = strtolower($name);

        return str_replace('DummyRoute', $route, $stub);
    }
    protected function replaceDummyModel($stub, $model)
    {
        return str_replace('DummyModel', $model, $stub);
    }
    protected function insertColumnFields($stub, $new)
    {
        return str_replace("$"."table->bigIncrements('id');", $new, $stub);
    }
    protected function replaceDummyGrid($stub, $grid)
    {
        return str_replace('DummyGrid', $grid, $stub);
    }
    protected function replaceDummyForm($stub, $form)
    {
        return str_replace('DummyForm', $form, $stub);
    }
    
    public function parseName($name)
    {
        return trim(str_replace(array('export ', '\'', '"'), '', $name));
    }
    public function getModelPath($name=null){
        $name = $this->parseName($name);
        return $this->getPath('Model/'.$name);
    }
    public function getModelBasePath(){
        return app_path() . '/Model';
    }
}
