<?php
use Illuminate\Http\Request;

	/*const define('REMIT_STATUS_SUBMIT','pending');
	const define('REMIT_STATUS_CONFIRMED','confirmed');*/

	//get current user
	function getCurrentUser(){
		if(Auth::user())
			return Auth::user();
	}

	
	// convert collection array to option array
	function toOptionArray($collection=array(),$field_name_as_label="name"){
		
        $options = array();
        
        foreach ($collection as $key => $value) {
            $options[] = array(
                    'value'=>$value['id'],
                    'label'=>$value[$field_name]
                );
        }
        return $options;
	}

	
	function authenticatePermissions($route_name){
		$permissions = Auth::user()->role->permissions;
		foreach ($permissions as $key => $permission) {
			# code...
			/*$route[] = "admin_" . $permission->permission_code .  ($permission->pivot->allow_view==1?"":"");
			$route[] = "admin_" . $permission->permission_code .  ($permission->pivot->allow_add==1?"_" . "add":"");
			$route[] = "admin_" . $permission->permission_code . ($permission->pivot->allow_edit==1?"_" . "edit":"");
			$route[] = "admin_" . $permission->permission_code . ($permission->pivot->allow_delete==1?"_" . "delete":"");*/
			$action = $permission->pivot->toArray();

			if($action['allow_view']){
				$route[] = "admin." . $permission->permission_code;
			}
			if($action['allow_add']){
				$route[] = "admin." . $permission->permission_code . ".add";
				$route[] = "admin." . $permission->permission_code . ".save";
			}
			if($action['allow_edit']){
				$route[] = "admin." . $permission->permission_code . ".edit";
				$route[] = "admin." . $permission->permission_code . ".update";
			}
			if($action['allow_delete']){
				$route[] = "admin." . $permission->permission_code . ".delete";
			}

			
		}
		$route[] = "admin.item.ajax.search";
		$route[] = "admin.customer.ajax.searchName";
		$route[] = "admin.pricerule.ajax.getApplicableRule";
		$route[] = "admin.pricerule.ajax.getApplicableRule";
		$route[] = "admin.sales.ajax.save";

		$route[] = "admin.pricerule.save";
		$route[] = "superadmin.customer.reindexCustomer";
		$route[] = "admin.import.importConvertItem";

		//dd($route_name);
		/*echo '=<pre>';
			print_r($route_name);
		echo '</pre>=';
		echo '=<pre>';
			print_r($route);
		echo '</pre>=';
		echo '=<pre>';
			print_r(in_array($route_name,$route));
		echo '</pre>=';
		exit();*/
		return in_array($route_name,$route);
	}
?>
