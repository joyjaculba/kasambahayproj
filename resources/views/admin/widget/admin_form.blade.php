@extends('layouts.app')

@section('content')

<div class="container pt-5">
	<div class="col-md-12">
		<h2>{{ $page_name }}</h2>
	</div>
    <div class="row">
    	<div class="col-md-12">
	    	@if (count($errors) > 0)
		        <div class="alert alert-danger">
		            <ul>
		                @foreach ($errors->all() as $error)
		                    <li>{{ $error }}</li>
		                @endforeach
		            </ul>
		        </div>
		    @endif
		</div>
    	<div class="col-md-12">
    		<form action="{{ $submit_url }}" method="post" multipart="form/enctype">
	        	{{ csrf_field() }}
		        <div class="row text-right form-group " data-spy="affix" data-offset-top="60" data-offset-bottom="200">
		        	<a href="{{ $back_url }}" class="btn btn-default btn-lg ml-auto">Back</a> &nbsp;
		        	<input type="submit" class="btn btn-success btn-lg" value="Save" /> &nbsp;
		        	<!-- <input type="submit" class="btn btn-success" value="Save and Continue"/> -->
		        </div>
		        <hr>
	            <div class="col-md-12 form-tabs">
	            
					<ul class="nav nav-tabs" role="tablist" style="margin-bottom:20px;">
						<li role="presentation" class="active">
							<a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a>
						</li>
						@if(sizeOf($extra_tabs))
							@foreach($extra_tabs as $tab)
								<li role="presentation">
									<a href="#{{$tab['id']}}" aria-controls="{{$tab['id']}}" role="tab" data-toggle="tab">{{$tab['label']}}</a>
								</li>
							@endforeach
						@endif
					</ul>
					<div class="tab-content clearfix">
	    				<div role="tabpanel" class="tab-pane clearfix active" id="general">
	    					<div class="col-md-12">
				            	@foreach($fieldset->getAllFields() as $field)
				            		<?php 
				            			$field['value'] = isset($field['value'])?$field['value'] : old($field['name']) 
				            		?>
				            		@if(isset($field['visible']) && $field['visible']==true)
					            		
						            	{{ App\Model\Forms\AdminFormAbstract::renderElement($field) }}
						            @elseif(isset($field['visible']) && $field['visible']==false)

						            @else
						            	{{ App\Model\Forms\AdminFormAbstract::renderElement($field) }}
						            @endif
				            	@endforeach
				            </div>

			            </div>
			            @if(sizeOf($extra_tabs))
							@foreach($extra_tabs as $tab)
								<div role="tabpanel" class="tab-pane" id="{{$tab['id']}}">
			            			@include($tab['resource_view'])
			            		</div>
							@endforeach
						@endif
			            
			        </div>
	            </div>
		               <!--  <table class="table">
							<tbody>
							
								<tr>
									<td>{{ $field["label"] }}</td>
									<td>{{ App\Model\Forms\AdminFormAbstract::renderElement($field) }}</td>
								</tr>
							
							</tbody>
						</table> -->
	    	</form>
	    </div>
    </div>
</div>
	
@endsection