@extends('layouts.app')

@section('content')

<div class="container pt-5">
	<div class="col-md-12">
		<h2>{{ $page_name }}</h2>
	</div>
    <div class="row">
    	<div class="col-md-12">
	    	@if (count($errors) > 0)
		        <div class="alert alert-danger">
		            <ul>
		                @foreach ($errors->all() as $error)
		                    <li>{{ $error }}</li>
		                @endforeach
		            </ul>
		        </div>
		    @endif
		</div>
    	<div class="col-md-12">
    		<form action="{{ $update_url }}" method="POST" multipart="form/enctype">
	        	{{ csrf_field() }}
    			<!-- <input type="hidden" name="_method" value="PUT"> -->
		        <div class="row text-right form-group" data-spy="affix" data-offset-top="60" data-offset-bottom="200">
		        	<a href="{{ $back_url }}" class="btn btn-secondary ml-auto">Back</a> &nbsp;
		        	@if(isset($can_delete))
		        		<a href="#" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger">Delete</a>
		        	@endif
		        	<input type="submit" class="btn btn-success" value="Update" /> &nbsp;
		        	<!-- <input type="submit" class="btn btn-success" value="Save and Continue"/> -->
		        </div>
		        <hr>
	            <div class="col-md-12 form-tabs">
	            	<ul class="nav nav-tabs" role="tablist" style="margin-bottom:20px;">
						<li role="presentation" class="active">
							<a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a>
						</li>
						@if(sizeOf($extra_tabs))
							@foreach($extra_tabs as $tab)
								<li role="presentation">
									<a href="#{{$tab['id']}}" aria-controls="{{$tab['id']}}" role="tab" data-toggle="tab">{{$tab['label']}}</a>
								</li>
							@endforeach
						@endif
					</ul>
					<div class="tab-content clearfix">
	    				<div role="tabpanel" class="tab-pane clearfix active" id="general">
	    					<div class="col-md-12">
				            	@foreach($fieldset->getAllFields() as $field)
				            	
				            		@if(isset($model[$field['name']]))
					            		<?php $field['value'] = $model[$field['name']] ?>
						            	{{ App\Model\Forms\AdminFormAbstract::renderElement($field) }}
						            @endif
				            	@endforeach
				            </div>
				        </div>
				        @if(sizeOf($extra_tabs))
							@foreach($extra_tabs as $tab)
								<div role="tabpanel" class="tab-pane" id="{{$tab['id']}}">
									@if(isset($tab['resource_view']) && $tab['resource_view'] !="")
			            				@include($tab['resource_view'],['id'=>$current_id])
			            			@endif
			            		</div>
							@endforeach
						@endif
				    </div>
	            </div>
		               <!--  <table class="table">
							<tbody>
							
								<tr>
									<td>{{ $field["label"] }}</td>
									<td>{{ App\Model\Forms\AdminFormAbstract::renderElement($field) }}</td>
								</tr>
							
							</tbody>
						</table> -->
	    	</form>
	    </div>
    </div>
</div>

	@if(isset($can_delete))
	<!-- Modal -->
		<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="deleteModal">Delete record</h4>
		      </div>
		      <div class="modal-body">
		        Are you sure you want to delete this record?
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <a href="{{$delete_url}}" type="button" class="btn btn-danger">Delete</a>
		      </div>
		    </div>
		  </div>
		</div>
	@endif
@endsection