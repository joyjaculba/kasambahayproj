<div class="form-group">
	<div class="row">
		@if(isset($data['grid-class'])) 
			<div class="{{$data['grid-class']==''?'col-md-12':$data['grid-class']}}">
		@else
			<div class="col-md-12">
		@endif
				<label for="{{ $data['name'] }}">
				 	{{ $data["label"] }}
				</label><br/>
				
					@if(isset($data['values']))
						@foreach($data['values'] as $_option)
							<input type="checkbox" name="{{ $data['name'] }}[]" value="{{ $_option['value'] }}" id="branch_{{ $_option['value'] }}"/> <label for="branch_{{ $_option['value'] }}">{{ $_option['label'] }}</label> &nbsp;&nbsp;&nbsp; 
						@endforeach
					@endif
				
			</div>
	</div>
</div>
<script type="text/javascript">
	var branch_ids = "{{$data['value']}}";
	branch_ids = branch_ids.split(",");
	branch_ids.forEach(function(el){
		$("#branch_"+el).attr("checked","checked");
	})
</script>