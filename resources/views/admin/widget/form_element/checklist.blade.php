<div class="form-group">
	<div class="row">
		@if(isset($data['grid-class'])) 
			<div class="{{$data['grid-class']==''?'col-md-12':$data['grid-class']}}">
		@else
			<div class="col-md-12">
		@endif
				<label for="{{ $data['name'] }}" class="" style="width:100%">
				 	{{ $data["label"] }}
				</label>	
				<input type="text" {{ isset($data['disabled'])&&$data['disabled']==true?"disabled":"" }} 
					class="form-control" id="{{ $data['id'] }}" name="{{ $data['name'] }}" 
					style="<?php echo isset($data['style'])?$data['style']:''; ?>"
					value="{{isset($data['value'])?$data['value']:''}}"
					readonly
					/>
				<br/>
				@if(isset($data['values']))
				<?php
					$current_values = explode(",", $data['value']);

				?>
					<ul class="list-unstyled list-inline">
						@foreach($data['values'] as $_option)
							<li class="col-md-3">
								<label>
								<input onclick="selectItem(this)" class="check-items" type="checkbox" value="{{ $_option['value'] }}"
									<?php echo in_array($_option['value'],$current_values)?'checked':'' ?>
								/>
								{{ $_option['label'] }}
								</label>
							</li>
						@endforeach
					</ul>
				@endif
			</div>
	</div>
</div>
<script type="text/javascript">
	function selectItem(obj){
		var temp = Array();
		var values = $(".check-items:checked").each(function(i,el){ console.log(el.value);temp.push(el.value) });
		$("#{{ $data['id'] }}").val(temp.join(","));
	}
</script>