<div class="form-group">
	<div class="row">
		@if(isset($data['grid-class'])) 
			<div class="{{$data['grid-class']==''?'col-md-12':$data['grid-class']}}">
		@else
			<div class="col-md-12">
		@endif
			<label>{{ $data["label"] }}</label>
			<div class="input-group">
				<!-- <div class="input-group-prepend">
					<span class="input-group-text" id="">{{ $data["label"] }}</span>
				</div> -->
				@foreach($data['input'] as $input)
					@if(isset($input['type']))
						@if($input['type']=="select")
						<select class="form-control" name="{{$input['name']}}" id="town">
							<option value="">Town/City, Zipcode</option>
							@if(isset($input['values']))
								@foreach($input['values'] as $_option)
								
									<option value="{{$_option->town}}, {{ $_option->zipcode}}">
										{{ $_option->town}}, {{ $_option->zipcode}}
									</option>
								@endforeach
							@endif
						@elseif($input['type']=="button")
							<input type="button" class="btn btn-primary col-md-2 w-100" name="{{$input['name']}}" id="{{$input['name']}}"value="{{$input['values']}}" />
						@endif
					@elseif(isset($input['disabled'])=="yes")
						<input type="text" class="form-control" placeholder="{{$input['placeholder']}}" name="{{$input['name']}}" disabled>

					@else
					<input type="text" class="form-control col-md-3 w-100" placeholder="{{$input['placeholder']}}" id="{{$input['name']}}" name="{{$input['name']}}" >
						@if(isset($input['append'])!="")
						<div class="input-group-append append-domainname">
						    <span class="input-group-text "><strong>.boholpages.com</strong></span>
						 </div>
						@endif
					@endif
					</select>
				@endforeach
			</div>
			<div class="pl-2 {{ $data['label'] }} help-block text-danger"></div>
	</div>
</div>
</div>