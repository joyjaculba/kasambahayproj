<div class="form-group">
	<div class="row">
		@if(isset($data['grid-class'])) 
			<div class="{{$data['grid-class']==''?'col-md-12':$data['grid-class']}}">
		@else
			<div class="col-md-12 ">
		@endif
				<label for="">
				 	<?php echo isset($data['label'])?$data['label']:''; ?> : 
				 </label>
				<?php echo isset($data['value'])?$data['value']:''; ?>
			</div>
	</div>
</div>