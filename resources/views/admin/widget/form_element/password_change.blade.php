<div class="form-group">
	<div class="">
		 <label for="{{ $data['name'] }}">
		 	{{ $data["label"] }}
		 </label>

		<input class="form-control" style="width:200px;" type="password" id="{{ $data['id'] }}" name="{{ $data['name'] }}" 
			style="<?php echo isset($data['style'])?$data['style']:''; ?>"
			value=""/>
	</div>
</div>