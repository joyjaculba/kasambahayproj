<div class="form-group">
	<div class="form-group">
		<label>
			{{ $data["label"] }}
		</label>
	</div>
	
	@foreach($data['values'] as $option)
		<label class="radio-inline">
			<input type="radio" name="{{$data['name']}}" value="{{$option}}">{{$option}}
		</label>
	@endforeach
</div>

