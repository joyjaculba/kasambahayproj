<div class="form-group">
	<div class="row">
		<div class="col-md-6">
	
			<label for="{{ $data['name'] }}">
			 	{{ $data["label"] }}
			</label>
			
				<table id="rebate_table" class="table table-striped table-bordered">
					<thead>
						<tr>
							<td>Qty from</td>
							<td>Qty to</td>
							<td>Rebate amout</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<input type="text" class="form-control" name="{{ $data['name'] }}[0][qty_from]">
							</td>
							<td>
								<input type="text" class="form-control" name="{{ $data['name'] }}[0][qty_to]">
							</td>
							<td>
								<input type="text" class="form-control" name="{{ $data['name'] }}[0][rebate_amount]">
							</td>
							<td>
								<button type="button" onclick="removeRow(this);return false;" class="btn btn-danger btn-sm">X</button>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="4" align="right">
								<button onclick="addNewRow()" type="button" class="btn btn-default">Add New</button>
							</td>
						</tr>
					</tfoot>
				</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	function removeRow(obj){
		$(obj).parent().parent().remove();
	}
	function addNewRow(){
		var new_row = "";
		var count = $("#rebate_table tbody tr").length;
		new_row +='	<tr>';
		new_row +='		<td>';
		new_row +='			<input type="text" class="form-control" name="{{ $data['name'] }}['+count+'][qty_from]">';
		new_row +='		</td>';
		new_row +='		<td>';
		new_row +='			<input type="text" class="form-control" name="{{ $data['name'] }}['+count+'][qty_to]">';
		new_row +='		</td>';
		new_row +='		<td>';
		new_row +='			<input type="text" class="form-control" name="{{ $data['name'] }}['+count+'][rebate_amount]">';
		new_row +='		</td>';
		new_row +='		<td>';
		new_row +='			<button type="button" onclick="removeRow(this);return false;" class="btn btn-danger btn-sm">X</button>';
		new_row +='		</td>';
		new_row +='	</tr>';

		$("#rebate_table > tbody").append(new_row);
	}
</script>