<div class="form-group">
	<div class="row">
		@if(isset($data['grid-class'])) 
			<div class="{{$data['grid-class']==''?'col-md-12':$data['grid-class']}}">
		@else
			<div class="col-md-12">
		@endif
				<label for="{{ $data['name'] }}">
				 	{{ $data["label"] }}
				</label>	
				<select {{ isset($data['disabled'])&&$data['disabled']==true?"disabled":"" }} class="form-control" id="{{ $data['id'] }}" name="{{ $data['name'] }}" 
					style="<?php echo isset($data['style'])?$data['style']:''; ?>" onchange="<?php echo isset($data['onchange'])?$data['onchange']:''; ?>">
					@if(isset($data['values']))
							<option value="">{{isset($data['empty_value_label']) ? $data['empty_value_label'] : ""}}</option>
						@foreach($data['values'] as $_option)
							<option value="{{$_option->id}}">
								{{ $_option->category }}
							</option>
						@endforeach
					@endif
				</select>
			</div>
	</div>
</div>
