<div class="form-group">
	<div class="row">
		@if(isset($data['grid-class'])) 
			<div class="{{$data['grid-class']==''?'col-md-12':$data['grid-class']}}">
		@else
			<div class="col-md-12">
		@endif
				<label for="{{ $data['name'] }}">
				 	{{ $data["label"] }}
				 </label>
				<input class="form-control" type="text" id="{{ $data['id'] }}" name="{{ $data['name'] }}" 
					style="<?php echo isset($data['style'])?$data['style']:''; ?>"
					value="<?php echo isset($data['value'])?$data['value']:''; ?>"/>
			</div>
	</div>
</div>