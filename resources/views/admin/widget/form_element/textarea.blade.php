<div class="form-group">
	<div class="row">
		@if(isset($data['grid-class'])) 
			<div class="{{$data['grid-class']==''?'col-md-12':$data['grid-class']}}">
		@else
			<div class="col-md-12">
		@endif
				<label for="{{ $data['name'] }}">
				 	{{ $data["label"] }}
				</label>
				<textarea class="form-control" id="{{ $data['id'] }}" name="{{ $data['name'] }}" 
					style="<?php echo isset($data['style'])?$data['style']:''; ?>" rows="<?php echo isset($data['rows'])?$data['rows']:''; ?>">@if(isset($data['value'])){{ $data['value'] }}@endif</textarea> 
		
			</div>
	</div>
</div>