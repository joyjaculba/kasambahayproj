<div class="form-group">
	<div class="row">
		<div class="form-group col-md-4">
			 <label for="{{ $data['name'] }}">
			 	{{ $data["label"] }}
			 </label>
			<select class="form-control" id="{{ $data['id'] }}" name="{{ $data['name'] }}" 
				style="<?php echo isset($data['style'])?$data['style']:''; ?>">
				<option value="1" <?php echo isset($data['value']) && $data['value']==1?"selected":""?> >Enable</option>
				<option value="0" <?php echo isset($data['value']) && $data['value']==0?"selected":""?> >Disable</option>
			</select>
		</div>
	</div>
</div>