@extends('layouts.app')

@section('content')

<div class="admin-content container pt-5">
	<div class="">
		<h2>{{ $page_name }}</h2>
		<hr/>
	</div>
    <div class="row">
    	<div class="col-md-12">
    		@if (session('success_message'))
		        <div class="alert alert-success">
		            {!! session('success_message') !!}
		        </div>
		    @endif
    	</div>
    	<div class="col-md-12 text-right" style="margin-bottom:10px;">
    		
    		
	    	
    		@if($allow_add)
    			<a href="{{ route($add_url) }}" class="btn btn-success">Add New</a>
    		@endif

    		@if(isset($has_custom_button) && $has_custom_button==true)
    			<a href="{{ isset($custom_button_url)?route($custom_button_url):'' }}" class="btn btn-success">{{isset($custom_button_text)?$custom_button_text:''}}</a>
    		@endif
    	</div>
    	@if(isset($has_search_bar) && $has_search_bar != '')
    		@include($has_search_bar);
    	@endif
    	<div class="col-md-12 text-right">
    		@if(isset($has_date_search) && $has_date_search == true)
	    		@include('admin.widget.grid.date_search',['route'=>$route->getUri()]);
    		@endif
    	</div>
    	<div class="col-md-12">
    		@if($collection instanceof \Illuminate\Pagination\AbstractPaginator)
	    		<div class="pull-left pagination-container" style="margin-bottom: 0px;">
		    		@if($collection->links('vendor.pagination.bootstrap-4')!==null)
		    			@if(isset($_GET['q']) && isset($_GET['ds']) && isset($_GET['de']))
		    				{{ $collection->appends(['q'=>$_GET['q'],'ds'=>$_GET['ds'],'de'=>$_GET['de']])->links() }}
		    			@elseif(isset($_GET['q']) && isset($_GET['search_index']) && isset($_GET['search_value']))
		    				{{ $collection->appends(['q'=>true,'search_index'=>$_GET['search_index'],'search_value'=>$_GET['search_value']])->links() }}
		    			@else
							{{ $collection->links('vendor.pagination.bootstrap-4') }}
						@endif
					@endif
	    		</div>
	    	@endif
    	</div>
    	@if($collection->count() > 0)
    	<div class="col-md-12">
    		<p>Total Number of Records: <strong>{{ $collection->total() }}</strong></p>
    	</div>
    	@endif
        <div class="col-md-12">
        	<?php
        		// dd($collection);
        		$current_url = Request::url();
        		if($collection){
        			$current_url = Request::url() . $collection->currentPage()?'?page='.$collection->currentPage():'?';
        		}

        	?>
            <div class="panel panel-default">
                <table class="table table-striped">
					<thead>
						<tr>
						@foreach($columns as $column)
							<th>
								@if(isset($column['index']))
									<?php 
										$direction = 'asc';
										if(isset($column['direction'])){
											$direction = $column['direction'];

										}
									?>

									<a href="{{$current_url . '&sort=' . $column['index'] . '&d=' . $direction }}">
										{{ $column['title'] }}
									</a>
								@else
									{{ $column['title'] }}
								@endif
							</th>
						@endforeach
							<th></th>
						</tr>
					</thead>
					<tbody>

					@foreach($collection as $item)
						<tr>
							@foreach($columns as $column)
								@if($column['index'] == 'status' && (isset($column['status_type']) && $column['status_type']==boolean))
									
									<td>{{ $item[$column['index']]==1?'enabled':'disabled' }}</td>
								@else
									@if(isset($column['map_value']))
										<td>{!! isset($column['map_value'][$item[$column['index']]])?$column['map_value'][$item[$column['index']]]:'' !!}</td>
									@else
										<td>{{ isset($item[$column['index']])?$item[$column['index']]:'' }}</td>
									@endif
								@endif
							@endforeach
								<td class="text-right">
									@if(isset($view_url) && $view_url !=null)
										@if(route($view_url,$item['id'])!=null)
											<a href=" {{ route($view_url,$item['id'])}}">
												<span class="glyphicon glyphicon-search"></span> View
											</a>
										@endif
									@endif
									@if($edit_url!="")
										@if($allow_edit)
											<a href=" {{ route($edit_url,$item['id'])}}">
												<span class="glyphicon glyphicon-pencil"></span> Edit
											</a>
										@endif
									 @endif
									<!-- <a href="{{ route($edit_url,$item['id'])}}">
										<span class="glyphicon glyphicon-trash"></span> delete
									</a> -->
								</td>
						</tr>
					@endforeach	
					</tbody>
				</table>
                </div>
            </div>
        </div>
    </div>
</div>
	
@endsection