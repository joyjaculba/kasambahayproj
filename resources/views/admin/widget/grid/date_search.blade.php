<div class="col-md-3 form-group">
	<div class="row">
		<div class="input-daterange input-group" id="datepicker">
		    <input type="text" class=" form-control" id="start" name="start" value="{{ $search_date_from }}"/>
		    <span class="input-group-addon">to</span>
		    <input type="text" class=" form-control" id="end" name="end" value="{{ $search_date_to }}"/>
		</div>
		<script type="text/javascript">
			$('.input-daterange').datepicker({
				format: "yyyy-mm-dd"
			});
		</script>
	</div>
</div>

<div class="col-md-3 form-group text-left">
	<button onclick="filterDate()" class="btn btn -sm  btn-info">SEARCH</button> <a href="{{ url($route) }}" class="btn btn -sm btn-link"><span class="glyphicon glyphicon-remove"></span> Clear</a>
</div>
<script type="text/javascript">
	function filterDate(){
		var start = $("#start").val();
		var end = $("#end").val();
		var url = "{{ url($route) }}?q=date&ds="+start+"&de="+end;
		window.location.href=url;
	}
</script>
    			