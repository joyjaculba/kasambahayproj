<div class="col-md-7 form-group">
	<div class="col-md-5 text-left">
		<div class="row">
			<label>Search</label>
			<input type="text" class=" form-control" id="search" name="search" value=""/>
		</div>
	</div>
	<div class="col-md-3 text-left">
		<div class="">
			<label>Search by :</label>
			<select id="search_by" class="search_by form-control">
				@foreach($search_indexes as $index)
					<option value="{{$index}}">{{$index}}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-md-4 text-left">
		<label style="width:100%">&nbsp;</label>
		<button class="btn btn-info" onclick="doSearch();">Submit</button>
		<button class="btn btn-link" onclick="resetSearch();">Reset</button>
	</div>
</div>
<script type="text/javascript">
	function doSearch(){
		var url = "{{url($route)}}";
		var q = jQuery("#search").val();
		var search_by = jQuery("#search_by").val();

		window.location.href=url+"?q=true&search_index="+search_by+"&search_value="+q;
	}
	function resetSearch(){
		var url = "{{url($route)}}";
		window.location.href=url;
	}
</script>