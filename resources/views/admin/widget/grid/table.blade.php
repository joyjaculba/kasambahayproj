<div class="col-md-12">
    <div class="panel panel-default">
        <table class="table">
			<thead>
				<tr>
				@foreach($columns as $column)
					<th>{{ $column['title'] }}</th>
				@endforeach
					<th></th>
				</tr>
			</thead>
			<tbody>
			@foreach($collection as $item)
				<tr>
					@foreach($columns as $column)
						@if($column['index'] == 'status' && (isset($column['status_type']) && $column['status_type']==boolean))
							<td>{{ $item[$column['index']]==1?'enabled':'disabled' }}</td>
						@else
							<td>{{ $item[$column['index']] }}</td>
						@endif
					@endforeach
						<td class="text-right">
							@if($edit_url!="")
								@if($allow_edit)
									<a href=" {{ route($edit_url,$item['id'])}}">
										<span class="glyphicon glyphicon-pencil"></span> edit
									</a>
								@endif
							 @endif
							<!-- <a href="{{ route($edit_url,$item['id'])}}">
								<span class="glyphicon glyphicon-trash"></span> delete
							</a> -->
						</td>
				</tr>
			@endforeach	
			</tbody>
		</table>
        </div>
    </div>
</div>