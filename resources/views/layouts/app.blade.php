<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('skin/default/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('skin/default/css/default.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark affix-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample03">
                        @guest
                        @else
                        @if(Auth::user()->role_id == 1)
                         
                        <ul class="navbar-nav font-weight-bold">
                            <li class="nav-item {{ (request()->is('admin/badges*')) ? 'active' : '' }}"><a class="nav-link" href="{{route('admin.badges')}}">Badges</a></li>
                            <li class="{{ (request()->is('admin/jobpreference*')) ? 'active' : '' }}"><a class="nav-link" href="{{route('admin.jobpreference')}}">Job Preferences</a></li>
                            <li class="{{ (request()->is('admin/documenttype*')) ? 'active' : '' }}"><a class="nav-link" href="{{route('admin.documenttype')}}">Document Type</a></li>
                        </ul>
                        @endif
                        @endguest
                         @if (Route::has('login'))
                        <ul class="navbar-nav ml-auto font-weight-bold">
                            @guest
                            <li class="nav-item {{ (request()->is('index')) ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('index')}}">Home</a>
                            </li>
                            <li class="nav-item {{ (request()->is('login')) ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('login') }}">Login</a>
                            </li>
                            <li class="nav-item {{ (request()->is('signup')) ? 'active' : '' }}">
                                <a class="nav-link" href="">Applicants</a>
                            </li>
                            <li class="nav-item  {{ (request()->is('employers')) ? 'active' : '' }}">
                                <a class="nav-link" href="">Employers</a>
                            </li>
                            <li class="nav-item {{ (request()->is('faq')) ? 'active' : '' }}">
                                 <a class="nav-link" href="">Abouts Us</a>
                            </li>
                            <li class="nav-item {{ (request()->is('faq')) ? 'active' : '' }}">
                                 <a class="nav-link" href="">Contact Us</a>
                            </li>
                            @else
                            <li class="nav-item dropdown" >
                                <a class="nav-link dropdown-toggle" href="" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</a>
                                    @if(Auth::user()->role_id == 1)
                                    <div class="dropdown-menu" aria-labelledby="dropdown03" style="left:-90px;">
                                        <a class="dropdown-item" href="{{ route('admin.account') }}">Manage Account</a>
                                    @else 
                                    <div class="dropdown-menu" aria-labelledby="dropdown03" style="left:-50px;">
                                    @endif
                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                    </div>
                            </li>
                            @endguest
                        </ul>
                    @endif
                    </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
