<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
	return view('kasambahay/default/layout/1column');
})->name('index');
// Route::get('/', 'HomeController@home')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin/applicant', 'ApplicantController@index')->name('admin.applicant');
Route::get('/admin/applicant/add', 'ApplicantController@add')->name('admin.applicant.add');
Route::get('/admin/applicant/edit/{id}', 'ApplicantController@edit')->name('admin.applicant.edit');
Route::post('/admin/applicant/save', 'ApplicantController@save')->name('admin.applicant.save');
Route::post('/admin/applicant/update/{id}', 'ApplicantController@update')->name('admin.applicant.update');

Route::get('/admin/employer', 'EmployerController@index')->name('admin.employer');
Route::get('/admin/employer/add', 'EmployerController@add')->name('admin.employer.add');
Route::get('/admin/employer/edit/{id}', 'EmployerController@edit')->name('admin.employer.edit');
Route::post('/admin/employer/save', 'EmployerController@save')->name('admin.employer.save');
Route::post('/admin/employer/update/{id}', 'EmployerController@update')->name('admin.employer.update');

Route::get('/admin/job', 'JobController@index')->name('admin.job');
Route::get('/admin/job/add', 'JobController@add')->name('admin.job.add');
Route::get('/admin/job/edit/{id}', 'JobController@edit')->name('admin.job.edit');
Route::post('/admin/job/save', 'JobController@save')->name('admin.job.save');
Route::post('/admin/job/update/{id}', 'JobController@update')->name('admin.job.update');

Route::get('/admin/documenttype', 'DocumentTypeController@index')->name('admin.documenttype');
Route::get('/admin/documenttype/add', 'DocumentTypeController@add')->name('admin.documenttype.add');
Route::get('/admin/documenttype/edit/{id}', 'DocumentTypeController@edit')->name('admin.documenttype.edit');
Route::post('/admin/documenttype/save', 'DocumentTypeController@save')->name('admin.documenttype.save');
Route::post('/admin/documenttype/update/{id}', 'DocumentTypeController@update')->name('admin.documenttype.update');


Route::get('/admin/jobpreference', 'JobPreferenceController@index')->name('admin.jobpreference');
Route::get('/admin/jobpreference/add', 'JobPreferenceController@add')->name('admin.jobpreference.add');
Route::get('/admin/jobpreference/edit/{id}', 'JobPreferenceController@edit')->name('admin.jobpreference.edit');
Route::post('/admin/jobpreference/save', 'JobPreferenceController@save')->name('admin.jobpreference.save');
Route::post('/admin/jobpreference/update/{id}', 'JobPreferenceController@update')->name('admin.jobpreference.update');

Route::get('/admin/documentupload', 'DocumentUploadController@index')->name('admin.documentupload');
Route::get('/admin/documentupload/add', 'DocumentUploadController@add')->name('admin.documentupload.add');
Route::get('/admin/documentupload/edit/{id}', 'DocumentUploadController@edit')->name('admin.documentupload.edit');
Route::post('/admin/documentupload/save', 'DocumentUploadController@save')->name('admin.documentupload.save');
Route::post('/admin/documentupload/update/{id}', 'DocumentUploadController@update')->name('admin.documentupload.update');

Route::get('/admin/jobapplication', 'JobApplicationController@index')->name('admin.jobapplication');
Route::get('/admin/jobapplication/add', 'JobApplicationController@add')->name('admin.jobapplication.add');
Route::get('/admin/jobapplication/edit/{id}', 'JobApplicationController@edit')->name('admin.jobapplication.edit');
Route::post('/admin/jobapplication/save', 'JobApplicationController@save')->name('admin.jobapplication.save');
Route::post('/admin/jobapplication/update/{id}', 'JobApplicationController@update')->name('admin.jobapplication.update');

Route::get('/admin/applicantreference', 'ApplicantReferenceController@index')->name('admin.applicantreference');
Route::get('/admin/applicantreference/add', 'ApplicantReferenceController@add')->name('admin.applicantreference.add');
Route::get('/admin/applicantreference/edit/{id}', 'ApplicantReferenceController@edit')->name('admin.applicantreference.edit');
Route::post('/admin/applicantreference/save', 'ApplicantReferenceController@save')->name('admin.applicantreference.save');
Route::post('/admin/applicantreference/update/{id}', 'ApplicantReferenceController@update')->name('admin.applicantreference.update');

Route::get('/admin/skills', 'SkillsController@index')->name('admin.skills');
Route::get('/admin/skills/add', 'SkillsController@add')->name('admin.skills.add');
Route::get('/admin/skills/edit/{id}', 'SkillsController@edit')->name('admin.skills.edit');
Route::post('/admin/skills/save', 'SkillsController@save')->name('admin.skills.save');
Route::post('/admin/skills/update/{id}', 'SkillsController@update')->name('admin.skills.update');

Route::get('/admin/languages', 'LanguagesController@index')->name('admin.languages');
Route::get('/admin/languages/add', 'LanguagesController@add')->name('admin.languages.add');
Route::get('/admin/languages/edit/{id}', 'LanguagesController@edit')->name('admin.languages.edit');
Route::post('/admin/languages/save', 'LanguagesController@save')->name('admin.languages.save');
Route::post('/admin/languages/update/{id}', 'LanguagesController@update')->name('admin.languages.update');

Route::get('/admin/workexeperience', 'WorkExperienceController@index')->name('admin.workexeperience');
Route::get('/admin/workexeperience/add', 'WorkExperienceController@add')->name('admin.workexeperience.add');
Route::get('/admin/workexeperience/edit/{id}', 'WorkExperienceController@edit')->name('admin.workexeperience.edit');
Route::post('/admin/workexeperience/save', 'WorkExperienceController@save')->name('admin.workexeperience.save');
Route::post('/admin/workexeperience/update/{id}', 'WorkExperienceController@update')->name('admin.workexeperience.update');

Route::get('/admin/education', 'EducationController@index')->name('admin.education');
Route::get('/admin/education/add', 'EducationController@add')->name('admin.education.add');
Route::get('/admin/education/edit/{id}', 'EducationController@edit')->name('admin.education.edit');
Route::post('/admin/education/save', 'EducationController@save')->name('admin.education.save');
Route::post('/admin/education/update/{id}', 'EducationController@update')->name('admin.education.update');

Route::get('/admin/badges', 'BadgesController@index')->name('admin.badges');
Route::get('/admin/badges/add', 'BadgesController@add')->name('admin.badges.add');
Route::get('/admin/badges/edit/{id}', 'BadgesController@edit')->name('admin.badges.edit');
Route::post('/admin/badges/save', 'BadgesController@save')->name('admin.badges.save');
Route::post('/admin/badges/update/{id}', 'BadgesController@update')->name('admin.badges.update');

Route::get('/admin/applicantbadges', 'ApplicantBadgesController@index')->name('admin.applicantbadges');
Route::get('/admin/applicantbadges/add', 'ApplicantBadgesController@add')->name('admin.applicantbadges.add');
Route::get('/admin/applicantbadges/edit/{id}', 'ApplicantBadgesController@edit')->name('admin.applicantbadges.edit');
Route::post('/admin/applicantbadges/save', 'ApplicantBadgesController@save')->name('admin.applicantbadges.save');
Route::post('/admin/applicantbadges/update/{id}', 'ApplicantBadgesController@update')->name('admin.applicantbadges.update');


Route::get('/admin/reviews', 'ReviewsController@index')->name('admin.reviews');
Route::get('/admin/reviews/add', 'ReviewsController@add')->name('admin.reviews.add');
Route::get('/admin/reviews/edit/{id}', 'ReviewsController@edit')->name('admin.reviews.edit');
Route::post('/admin/reviews/save', 'ReviewsController@save')->name('admin.reviews.save');
Route::post('/admin/reviews/update/{id}', 'ReviewsController@update')->name('admin.reviews.update');





